<?php
/**
 * Component Name: Services
 *
 * Component for displaying services as parallelogram shape
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$button = get_sub_field('section_button');
$i = 0;
?>
<section id="what-i-do" class="fc fc--services services section-bg--primary no-padding">
  <ul class="parallelogram-list">
    <?php
    if( have_rows('services_item') ):
      while ( have_rows('services_item') ) : the_row();

      $icon = get_sub_field('service_icon');
      $i++;
    ?>
      <li class="parallelogram-list__item">
        <div class="parallelogram-list__content">
          <?php if ($icon) : ?>

            <div class="services__icon-container">
              <i class="services__icon <?= $icon ?>"></i>
            </div>

          <?php endif ?>

          <?php  if ( $i === 1 ) : ?>
             <h1><?= the_sub_field('service_title') ?></h1>
          <?php else: ?>
            <h2 class="service-title"><?= the_sub_field('service_title') ?></h2>
          <?php endif; ?>

          <?php the_sub_field('service_content'); ?>

          <?php  if ( $i === 1 ) : ?>
            <span class='arrow-right'></span>
          <?php endif; ?>

        </div>
      </li><!-- .parallelogram-list__item -->
    <?php
      endwhile;
    endif;
    ?>
    <!--
    <li class="parallelogram-list__item">
      <div class="parallelogram-list__item__content">
        <h1>What I Do</h1>
        <p>I'm working hard to master all geeky stuff needed to <strong>push your business to the next level</strong>, so I'm eager to help you with: </p>
        <span class="arrow-right"></span>
      </div>
    </li>
    <li class="parallelogram-list__item">
      <div class="parallelogram-list__item__content">
        <div class="fc--services__icon-container">
          <i class="fc--services__icon design-icon"></i>
        </div>
        <h2 class="service-title">Web Design</h2>
        <p>Do you want unique, bespoke and clean (re)design to stand out of your competitors?</p>
        <p>Let's start with drawing the mockups to create some UI, and then continue further to <strong>skin every single element</strong> of your app/website! I will analyze it for best possible balance between your wishes and <strong>UX</strong>.</p>

      </div>
    </li>
    <li class="parallelogram-list__item">
      <div class="parallelogram-list__item__content">
        <div class="fc--services__icon-container">
          <i class="fc--services__icon development-icon"></i>
        </div>
        <h2 class="service-title">Web Development</h2>
        <p>Creating a website from scratch, or establishing your online presence with WordPress.</p><p>Did you know that <strong>more than 30% of the whole web</strong> is powered with WordPress? Custom WordPress theme development is probably the golden middle between cost, speed and quality on the market today!</p>

      </div>
    </li>
    <li class="parallelogram-list__item">
      <div class="parallelogram-list__item__content">
        <div class="fc--services__icon-container">
          <i class="fc--services__icon optimization-icon"></i>
        </div>
        <h2 class="service-title">Web Optimization</h2>
        <p>Any custom task that you need completed, ranging from moving the clouds around (I can give a try at least :D), to small design fixes, making your site mobile-friendly, speed optimization (rated with anything but "A" on <a target="_blank" href="http://gtmetrix.com">GTmetrix</a> is unacceptable), Search Engine Optimization to improve your rankings on Google...</p>

      </div>
    </li><!-- .parallelogram-list__item -->
  </ul><!-- .parallelogram-list -->
</section><!-- .hero -->
