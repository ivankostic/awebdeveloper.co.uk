<?php
/**
 * Footer Dynamic widget area
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
?>

<?php if ( is_active_sidebar( 'footer-dynamic' ) ) : ?>
<div class="footer-dynamic">
	<div class="<?php echo esc_attr( $container ); ?>" tabindex="-1">
		<div class="row">

			<?php dynamic_sidebar( 'footer-dynamic' ); ?>

		</div>
	</div>
</div><!-- .footer-dynamic -->
<?php endif; ?>
