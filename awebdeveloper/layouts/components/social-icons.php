<?php
/**
 * Partial template for displaying social icons
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$theme_options_page_id = 441;
?>

<ul class="social-icons">
	<?php if ( get_field('facebook', $theme_options_page_id) ) : ?>
  	<li class="social-icon"><a target="_blank" class="icon social-icon--facebook" href="<?= do_shortcode( '[options field=facebook]' ); ?>"></a></li>
	<?php endif; ?>
	<?php if ( get_field('twitter', $theme_options_page_id) ) : ?>
  <li class="social-icon"><a target="_blank" class="icon social-icon--twitter" href="<?= do_shortcode( '[options field=twitter]' ); ?>"></a></li>
	<?php endif; ?>
	<?php if ( get_field('instagram', $theme_options_page_id) ) : ?>
	<li class="social-icon"><a target="_blank" class="icon social-icon--instagram" href="<?= do_shortcode( '[options field=instagram]' ); ?>"></a></li>
	<?php endif; ?>
	<?php if ( get_field('linkedin', $theme_options_page_id) ) : ?>
  <li class="social-icon"><a target="_blank" class="icon social-icon--linkedin" href="<?= do_shortcode( '[options field=linkedin]' ); ?>"></a></li>
	<?php endif; ?>
	<?php if ( get_field('whatsapp', $theme_options_page_id) ) : ?>
	<li class="social-icon"><a target="_blank" class="icon social-icon--whatsapp" href="<?= do_shortcode( '[options field=whatsapp]' ); ?>"></a></li>
	<?php endif; ?>
</ul>
