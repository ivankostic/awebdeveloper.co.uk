<?php
/**
 * Template for displaying top bar in header
 *
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
?>
<div class="header-top-bar section-bg--dark">
  <div class="<?php echo esc_attr( $container ); ?>">
    <div class="row">
      <div class="col-md-12">
				<?php get_template_part( "layouts/components/social-icons" ); ?>
      </div>
    </div>
  </div>
</div><!-- .header-top-bar -->
