<?php
/**
 * Component Name: Hero
 *
 * Component for displaying a hero content.
 *
 * @package imwp
 *   <?= $hero_bg ? "style='background-image: url(" . $hero_bg[sizes][large] . ")'" : ""?>
 *   <img src="<?php echo get_image_src($hero_bg[ID], 'large') ?>" alt= "<?php ?>" />
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$hero_img   = get_sub_field('hero_background');
$hero_title = get_sub_field('hero_title');
//print_r($hero_img);
?>
<section class="fc fc--hero hero section-bg--primary">
  <canvas id="canvas-stars"></canvas>
  <div class="container">
    <div class="row">
      <div class="text-center text-md-left col-sm-12 col-lg-7">
        <?php if( $hero_title ) : ?>
          <h2 class="hero__title"><?= $hero_title; ?></h2>
        <?php endif; ?>
        <?php echo get_sub_field('hero_content'); ?>
      </div>
      <div class="col-lg-5">
        <img class="hero__image" src="<?= $hero_img['url'] ?>" alt="<?= $hero_img['alt'] ?>" width="<?= $hero_img['width'] ?>" height="<?= $hero_img['height'] ?>" />
      </div>
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .hero -->
