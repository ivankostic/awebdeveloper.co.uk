<?php
/**
 * Partial template for box with squeres effect
 *
 * @package imwp
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$portfolio_image = get_the_post_thumbnail_url( get_the_ID(), 'portfolio-thumb' );
?>
<div class="box box--sqaures-effect">
  <figure class="box--sqaures-effect__image"
  <?= $portfolio_image ? "style='background-image: url(" . $portfolio_image . ")'" : ""?>>
  </figure>
  <div class="box--sqaures-effect__overlay"></div>
  <div class="box--sqaures-effect__content">
    <h2 class="box--sqaures-effect__title"><?= get_the_title(); ?></h2>
    <p>Website Type: <span><?= get_field('website_category', get_the_ID()) ?></span></p>
    <a class="btn btn-primary" href="<?= get_the_permalink() ?>">Explore more</a>
  </div>
</div>
