<?php
/**
 * Partial template for displaying CTA
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
// Get CTA id from regular loop / current page
$cta_id =  get_field( 'cta_id' );

// Setting up loop for calling specific CTA from it's CPT
wp_reset_query();
$args = array(
  'post_type' => 'cta',
  'posts_per_page' => 1,
  'post_status' => 'publish',
  'p' => $cta_id[0]
);
$the_query = new WP_Query( $args );

while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<div class="cta cta--<?= get_the_ID(); ?> section-bg--primary">
  <div class="<?php echo esc_attr( $container ); ?>" >
    <div class="row">
      <div class="col-md-12">
        <h2 class="cta__title"><?php the_title(); ?></h2>
        <?php the_content(); ?>
        <?php
        $button = get_field( "cta_button");

        if ($button) :

         $btn = explode("@", $button);
         $button_label = $btn[0];
         $button_url   = $btn[1];
       ?>
         <a href="<?= $button_url ?>" class="btn btn-transparent"><?= $button_label ?></a>
       <?php
         endif;
       ?>
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- .entry-content -->
</div><!-- .cta -->
<?php
endwhile;
wp_reset_query();
?>
