<?php
/**
 * Component Name: Portfolio
 *
 * Component for displaying portfolio boxes
 *
 * @package imwp
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$title  = get_sub_field('section_title');
$button = get_sub_field('section_button');
$portfolio_ids_arr = get_sub_field('project');

$args = array(
  'post_type' => 'portfolio',
  'post_status' => 'publish',
  'post__in' => $portfolio_ids_arr,
  'order' => 'asc'
);

$the_query = new WP_Query( $args );

?>
<section id="portfolio" class="fc fc--portfolio">
  <?php ?>
  <div class="container">

    <?php if ($title) : ?>
    <div class="row">
      <div class="col-md-12">
        <?= $title ? "<h1 class='fc__title'>" . $title . "</h1>" : ""; ?>
      </div>
    </div>
    <?php endif ?>

    <div class="row">
      <?php
      if( $portfolio_ids_arr ):
        while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
        <div class="col-md-6 col-lg-6">
          <?php get_template_part('layouts/components/box', 'squares-effect'); ?>
        </div>
      <?php
        endwhile;
      endif;
      ?>
    </div><!-- .row -->

    <?php
     if ($button) :

      $btn = explode("@", $button);
      $button_label = $btn[0];
      $button_url   = $btn[1];
    ?>
    <div class="row">
      <div class="col-md-12 text-center">
        <a href="<?= $button_url ?>" class="btn btn-secondary fc__button"><?= $button_label ?></a>
      </div>
    </div>
    <?php
      endif;
    ?>
  </div><!-- .container -->
</section><!-- .portfolio -->
<?php wp_reset_query(); ?>
