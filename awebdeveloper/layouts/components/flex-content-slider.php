<?php
/**
 * Component Name: Content Slider
 *
 * Component for displaying a content in Bootstrap Slider.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$title  = get_sub_field('section_title');
$button = get_sub_field('section_button');
?>
<section class="fc fc--content-slider section-bg--light">

<?php if ($title) : ?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <?= $title ? "<h1 class='fc__title'>" . $title . "</h1>" : ""; ?>
    </div>
  </div>
</div>
<?php endif ?>

<?php
// check if the repeater field has rows of data
if( have_rows('slide') ): ?>

<div id="carousel-controls" class="content-slide carousel slide" data-ride="carousel">
  <?php
 	// loop through the rows of data
  $i = 1;

  while ( have_rows('slide') ) : the_row();
  ?>

  <div class="carousel-item <?= $i === 1 ? 'active' : '' ?>">
    <div class="content-slide__item text-center">
      <div class="content-slide__editor">
        <?php the_sub_field('slide_content'); ?>
      </div>
      <?php if ( get_sub_field(secondary_slide_content) ) : ?>

        <div class="content-slide__secondary">
          <div class="ratings">
              <span class="ratings__star">&#9733;</span><span class="ratings__star">&#9733;</span>
              <span class="ratings__star">&#9733;</span><span class="ratings__star">&#9733;</span>
              <span class="ratings__star">&#9733;</span>
          </div>
          <?php the_sub_field('secondary_slide_content'); ?>
        </div>

      <?php endif; ?>
    </div>
  </div><!-- .carousel-item -->

  <?php
  $i++;
  endwhile;
  ?>

  <ul class="carousel-indicators">
    <li data-target="#carousel-controls" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-controls" data-slide-to="1"></li>
    <li data-target="#carousel-controls" data-slide-to="2"></li>
    <li data-target="#carousel-controls" data-slide-to="3"></li>
  </ul>

	<ul class="carousel-navigation">
		<li>
			<a class="carousel-control-prev" href="#carousel-controls" role="button" data-slide="prev">
		   <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		   <span class="sr-only">Previous</span>
	  	</a>
		</li>
	  <li>
			<a class="carousel-control-next" href="#carousel-controls" role="button" data-slide="next">
		   <span class="carousel-control-next-icon" aria-hidden="true"></span>
		   <span class="sr-only">next</span>
		 </a>
		</li>
	</ul>

</div><!-- .content-slide .carousel -->

<?php
endif;
?>

  <?php
   if ($button) :

    $btn = explode("@", $button);
    $button_label = $btn[0];
    $button_url   = $btn[1];
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <a href="<?= $button_url ?>" class="btn btn-success fc__button"><?= $button_label ?></a>
      </div>
    </div>
  </div>
  <?php
    endif;
  ?>

</section>
