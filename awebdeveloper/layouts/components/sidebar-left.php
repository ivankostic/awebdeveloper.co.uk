<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( 'left-sidebar' ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = get_theme_mod( 'imwp_sidebar_position' );
?>

<?php if ( 'both' === $sidebar_pos || is_page_template( 'templates/both-sidebars.php' ) ) : ?>
	<div class="col-md-3 widget-area" id="left-sidebar" role="complementary">
<?php else : ?>
	<div class="col-md-4 widget-area" id="left-sidebar" role="complementary">
<?php endif; ?>

<?php dynamic_sidebar( 'left-sidebar' ); ?>

</div><!-- #left-sidebar -->
