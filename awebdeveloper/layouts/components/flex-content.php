<?php
/**
 * Component Name: Content editor
 *
 * Component for displaying just a normal content editor.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?>
<section class="fc fc--content-editor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php
          echo get_sub_field('content_editor');
        ?>
      </div>
    </div>
  </div>
</section>
