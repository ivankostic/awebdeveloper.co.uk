<?php
/**
 * Partial template for content in page.php
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
		the_content();
		edit_post_link( __( 'Edit', 'imwp' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php
		if ( get_field( 'cta_id' ) ) {
			get_template_part( 'layouts/components/cta');
		}
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
