<?php
/**
 * Partial template for portfolio content in portfolio.php
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );

$args = array(
  'post_type' => 'portfolio',
  'post_status' => 'publish',
  'posts_per_page ' => -1,
  'order' => 'asc'
);

$the_query = new WP_Query( $args );
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header entry-header--primary">
		<div class="<?php echo esc_attr( $container ); ?>" >
			<div class="row">
				<div class="col-md-12">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-featured-image">
			<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
		</div>
	<?php endif; ?>

	<div class="entry-content">
		<div class="<?php echo esc_attr( $container ); ?>" >
			<div class="row">
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
		<div class="row">
			<?php
				while ( $the_query->have_posts() ) : $the_query->the_post();
			?>
				<div class="col-md-6 col-lg-6">
					<?php get_template_part('layouts/components/box', 'squares-effect'); ?>
				</div>
			<?php
				endwhile;
				edit_post_link( __( 'Edit', 'imwp' ), '<span class="edit-link">', '</span>' );
			?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
		if ( get_field( 'cta_id' ) ) {
			get_template_part( 'layouts/components/cta');
		}
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
