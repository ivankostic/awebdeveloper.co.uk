<?php
/**
 * Search results partial template.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title  entry-title--archive"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta">

						<?php imwp_posted_on(); ?>

					</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-featured-image">
			<?php echo get_the_post_thumbnail( $post->ID, 'entry-post' ); ?>
		</div>
	<?php endif; ?>

	<div class="entry-summary">

		<?php the_excerpt(); ?>

	</div><!-- .entry-summary -->
	<footer class="entry-footer">

		<?php imwp_entry_footer(); ?>

	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
