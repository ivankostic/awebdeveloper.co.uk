<?php
/**
 * Partial template for content when some of page templates are selected
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<header class="entry-header entry-header--primary">
		<div class="<?php echo esc_attr( $container ); ?>" >
			<div class="row">
				<div class="col-md-12">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<div class="<?php echo esc_attr( $container ); ?>" >
			<div class="row">
				<div class="col-md-12">
					<?php
					the_content();
					edit_post_link( __( 'Edit', 'imwp' ), '<span class="edit-link">', '</span>' );
					?>
				</div>
			</div>
		</div>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php
		if ( get_field( 'cta_id' ) ) {
			get_template_part( 'layouts/components/cta');
		}
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
