<?php
/**
 * Portfolio - Single template
 *
 * Template for displaying portfolio custom post type items
 * Icons: <i class="code-sprite code-sprite--wp"></i><i class="code-sprite code-sprite--bs"></i><i class="code-sprite code-sprite--html"></i><i class="code-sprite code-sprite--css"></i><i class="code-sprite code-sprite--js"></i><i class="code-sprite code-sprite--jquery"></i><i class="code-sprite code-sprite--php"></i><i class="code-sprite code-sprite--mysql"></i>
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'imwp_container_type' );
?>

<div class="content-area" id="primary">

<?php while ( have_posts() ) : the_post(); ?>

	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		<header class="entry-header entry-header--portfolio-single"
		style="">
			<div class="<?php echo esc_attr( $container ); ?>" >

				<?php the_title( '<h1 class="entry-title entry-title--portfolio-single">', '</h1>' ); ?>
					<div class="row">
						<div class="col-md-5">
						<?php if ( has_post_thumbnail() ) : ?>

								<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

						<?php endif; ?>
						</div>
						<div class="col-md-7 brief">
							<?= get_field( "project_brief" ); ?>
							<?php
							$skills = get_field( "skills" );
							$icons = Array();

							echo "<p>";

							foreach ($skills as $skill) {
								 echo $skill[label] . ", ";

								 // Collect values of class parts into array
								 array_push( $icons, "<i class='code-sprite code-sprite--" . $skill[value] . "'></i>" );
							}

							echo "</p>";

							?>
						</div>
					</div><!-- .row -->
					<div class="row">
						<div class="col-md-12 text-center">
							<p class="skills-icons">
							<?php
							// Display icons
						  foreach ($icons as $icon) { echo $icon;  }
							?>
							</p>
						</div>
					</div><!-- .row -->
			</div><!-- .container -->
		</header><!-- .entry-header -->

		<div class="entry-content">
			<div class="<?php echo esc_attr( $container ); ?>" >
				<div class="row">
					<div class="col-md-12">
						<section class="portfolio-section">
							<?php the_content(); ?>
						</section>
						<section class="portfolio-section top-primary-border">
							<div class="row">
								<div class="col-md-8 offset-md-2">
									<h2 class="portfolio-heading"><?= do_shortcode( '[options field=project_features_title]' ); ?></h2>
									<?= get_field( "project_features" ); ?>
									<?= do_shortcode( '[options field=project_features_content]' ); ?>
								</div>
							</div>
						</section>
						<section class="portfolio-section top-primary-border">
							<div class="row">
								<div class="col-md-8 offset-md-2 text-center">
									<h2 class="portfolio-heading"><?= do_shortcode( '[options field=project_responsive_title]' ); ?></h2>
									<?= do_shortcode( '[options field=project_responsive_content]' ); ?>
									<?php if ( $project_dekstop_scr = get_field( "project_dekstop_scr" ) ) : ?>
													<img <?php echo get_image_src( $project_dekstop_scr[ID], 'full' ) ?> alt="Site view on Desktop" />
									<?php endif; ?>
									&nbsp;
									<?php if ( $project_mobile_scr = get_field( "project_mobile_scr" ) ) : ?>
													<img <?php echo get_image_src( $project_mobile_scr[ID], 'full' ) ?> alt="Site view on Tablet and Mobile" />
									<?php endif; ?>
								</div>
							</div>
						</section>
						<section class="portfolio-section">
							<?= do_shortcode( '[options field=project_bottom_content]' ); ?>
						</section>
					</div><!-- .col -->
				<?php edit_post_link( __( 'Edit', 'imwp' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php
			if ( get_field( 'cta_id' ) ) {
				get_template_part( 'layouts/components/cta');
			}
			?>
		</footer><!-- .entry-footer -->

	</article><!-- #post-## -->

	<?php endwhile; // end of the loop. ?>
</div><!-- #primary -->

<?php get_footer(); ?>
