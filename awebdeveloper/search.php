<?php
/**
 * The template for displaying search results pages.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'imwp_container_type' );
?>

<div class="<?php echo esc_attr( $container ); ?>"  tabindex="-1">
	<div class="row">

		<?php left_sidebar_check(); ?>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
					<h1 class="page-title"><?php printf(
					/* translators:*/
					 esc_html__( 'Search Results for: %s', 'imwp' ),
						'<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'layouts/loops/content', 'search' );
				?>
			<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( 'layouts/loops/content', 'none' ); ?>

		<?php endif; ?>

		<?php imwp_pagination(); ?>

	<?php right_sidebar_check(); ?>

	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
