<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'imwp_container_type' );
?>
<div class="<?php echo esc_attr( $container ); ?>"  tabindex="-1">
	<div class="row">

		<?php left_sidebar_check(); ?>

		<header class="page-header author-header">

			<?php
			$curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
			$author_name ) : get_userdata( intval( $author ) );
			?>

			<h1><?php esc_html_e( 'About:', 'imwp' ); ?><?php echo esc_html( $curauth->nickname ); ?></h1>

			<?php if ( ! empty( $curauth->display_name ) ) : ?>
				<h2><?php echo $curauth->display_name; ?></h2>
			<?php endif; ?>

			<dl>
				<?php if ( ! empty( $curauth->user_url ) ) : ?>
					<dt><?php esc_html_e( 'Website', 'imwp' ); ?></dt>
					<dd>
						<a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_html( $curauth->user_url ); ?></a>
					</dd>
				<?php endif; ?>

				<?php if ( ! empty( $curauth->user_description ) ) : ?>
					<dt><?php esc_html_e( 'Profile', 'imwp' ); ?></dt>
					<dd><?php echo esc_html( $curauth->user_description ); ?></dd>
				<?php endif; ?>
			</dl>

			<h2><?php esc_html_e( 'Posts by', 'imwp' ); ?> <?php echo esc_html( $curauth->nickname ); ?>
				:</h2>

		</header><!-- .page-header -->

		<ul>
			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<li>
						<a rel="bookmark" href="<?php the_permalink() ?>"
						   title="<?php esc_html_e( 'Permanent Link:', 'imwp' ); ?> <?php the_title(); ?>">
							<?php the_title(); ?></a>,
						<?php imwp_posted_on(); ?> <?php esc_html_e( 'in',
						'imwp' ); ?> <?php the_category( '&' ); ?>
					</li>
				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'layouts/loops/content', 'none' ); ?>

			<?php endif; ?>
		</ul>

	<?php imwp_pagination(); ?>

	<?php right_sidebar_check(); ?>

	</div> <!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
