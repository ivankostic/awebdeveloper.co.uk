<?php
/**
 * The template for displaying all single posts.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'imwp_container_type' );
?>

<div class="<?php echo esc_attr( $container ); ?>"  tabindex="-1">
	<div class="row">

	<?php left_sidebar_check(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'layouts/loops/content', 'single' ); ?>

			<?php imwp_post_nav(); ?>

		<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>

	<?php endwhile; // end of the loop. ?>

	<?php right_sidebar_check(); ?>

	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
