<?php
/**
 * Template Name: Full Width Layout
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>
<div class="content-area" id="primary">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'layouts/loops/content', 'page-with-containers' ); ?>

	<?php endwhile; // end of the loop. ?>
</div><!-- #primary -->

<?php get_footer(); ?>
