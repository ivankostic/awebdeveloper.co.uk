<?php
/**
 * Template Name: Portfolio
 *
 * Template for displaying portfolio items from Portfolio CPT
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>
<div class="content-area" id="primary">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'layouts/loops/content', 'portfolio' ); ?>

	<?php endwhile; // end of the loop. ?>
</div><!-- #primary -->

<?php get_footer(); ?>
