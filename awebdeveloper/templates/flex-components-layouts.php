<?php
/**
 * Template Name: Flex Components Layouts
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$id = get_the_ID();
?>
<?php //echo do_shortcode( '[Sassy_Social_Share type="floating" top="200" left="-10"]' ); ?>
<div class="content-area" id="primary">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php
			// ACF flexible content
			if(  have_rows('components', $id) ):
				while ( have_rows('components', $id) ) : the_row();

						get_template_part( 'layouts/components/flex', get_row_layout() );

				endwhile; // close the loop of flexible content
			endif; // close flexible content conditional
			?>

		<?php endwhile; // end of the loop. ?>

</div><!-- #primary -->

<?php get_footer(); ?>
