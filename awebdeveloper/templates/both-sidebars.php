<?php
/**
 * Template Name: Left and Right Sidebar Layout
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'imwp_container_type' );
?>
<div class="<?php echo esc_attr( $container ); ?>" >
	<div class="row">

		<?php get_template_part( 'layouts/components/sidebar', 'left' ); ?>

		<div
			class="<?php
				if ( is_active_sidebar( 'left-sidebar' ) xor is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-8<?php
				elseif ( is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-6<?php
				else : ?>col-md-12<?php
				endif; ?> content-area" id="primary"
			id="primary">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'layouts/loops/content', 'page' ); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>
		</div><!-- #primary -->

		<?php get_template_part( 'layouts/components/sidebar', 'right' ); ?>

	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
