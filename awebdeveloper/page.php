<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'imwp_container_type' );
?>
<div class="<?php echo esc_attr( $container ); ?>"  tabindex="-1">
	<div class="row">

		<?php left_sidebar_check(); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'layouts/loops/content', 'page' ); ?>

		<?php endwhile; // end of the loop. ?>

		<?php right_sidebar_check(); ?>

	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
