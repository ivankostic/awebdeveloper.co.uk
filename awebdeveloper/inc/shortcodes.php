<?php
/* ------------------------------------------------------------------------ */
/* Shortcode [recent_posts_with_thumbs num="3"]
/* ------------------------------------------------------------------------ */
function recent_posts_with_thumbs($atts, $content = null)
{
    extract( shortcode_atts( array(
      'num'      => '3',
    ),
      $atts )
    );

    $html = '<ul class="recent-posts-with-thumbnails widget_recent_entries">';

    // Define our WP Query Parameters
    $args = array (
     'post_type'      => 'post',
     'post_status'    => 'publish',
     'posts_per_page' => $num,
     'ignore_sticky_posts' => true
    );
    $the_query = new WP_Query( $args );

    // Start our WP Query
    while ($the_query -> have_posts()) : $the_query -> the_post();

    // Display the Post Title with Hyperlink
    $html .= '<li class="recent-posts-with-thumbnails__item">';
    $html .= '  <a href="'. get_permalink() .'">'. get_the_post_thumbnail( $post_id, 'recent-posts-thumb', array( 'class' => 'alignleft recent-posts-with-thumbnails__thumb' ) ) . '</a>';
    $html .= '  <div class="recent-posts-with-thumbnails__content">';
    $html .= '    <h4 class="recent-posts-with-thumbnails__title"><a href="'. get_permalink() .'">'. get_the_title() .'</a></h4>';
    $html .= '    <span class="post-date">'. get_the_date() .'</span>';
    $html .= '  </div>';
    $html .= '</li>';

    // Repeat the process and reset once it hits the limit
    endwhile;
    wp_reset_postdata();

    $html .= '</ul>';

    return $html;
}
add_shortcode('recent_posts_with_thumbs','recent_posts_with_thumbs');


/* ------------------------------------------------------------------------ */
/* Shortcode [options field="field_name"]
/* ------------------------------------------------------------------------ */
function options($atts, $content = null)
{
    extract( shortcode_atts( array(
      'field'      => '',
    ),
      $atts )
    );

    if ($field) {

      // Define our WP Query Parameters
      $args = array (
       'post_type'      => 'theme_options',
       'post_status'    => 'publish',
       'posts_per_page' => 1,
       'ignore_sticky_posts' => true
      );
      $the_query = new WP_Query( $args );

      // Start our WP Query
      while ($the_query -> have_posts()) : $the_query -> the_post();

        $f = get_field($field);
        // var_dump($f);

        $html = $f;

      // Repeat the process and reset once it hits the limit
      endwhile;
      wp_reset_postdata();

      return $html;
    }

}
add_shortcode('options','options');
