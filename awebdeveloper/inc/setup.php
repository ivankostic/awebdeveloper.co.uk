<?php
/**
 * Theme basic setup.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'imwp_setup' );
if ( ! function_exists ( 'imwp_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function imwp_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on imwp, use a find and replace
		 * to change 'imwp' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'imwp', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'imwp' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'entry-post', 730, 360, true ); // (cropped)
		add_image_size( 'portfolio-thumb', 560, 399, true ); // (cropped)
		add_image_size( 'recent-posts-thumb', 64, 64, true ); // (cropped)

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'imwp_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );

		// Check and setup theme default settings.
		imwp_setup_theme_default_settings();

	}
}

function custom_post_type() {

  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Portfolio' ),
        'singular_name' => __( 'Portfolio' )
      ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
      'public' => true,
      'menu_icon' => 'dashicons-layout',
      'exclude_from_search' => true,
    )
  );

	register_post_type( 'cta',
		array(
			'labels' => array(
				'name' => __( 'Call to Actions' ),
				'singular_name' => __( 'Call to Action' )
			),
			'supports' => array( 'title', 'editor', 'revisions' ),
			'public' => true,
			'menu_icon' => 'dashicons-share-alt2',
			'exclude_from_search' => true,
		)
	);

	register_post_type( 'theme_options',
		array(
			'labels' => array(
				'name' => __( 'Theme options' ),
				'singular_name' => __( 'Theme options' )
			),
			'supports' => array( 'title', 'editor', 'revisions' ),
			'public' => true,
			'menu_icon' => 'dashicons-admin-tools',
			'exclude_from_search' => true,
			'capability_type' => 'post',
		  'capabilities' => array(
		    'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
		  ),
		  'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
		)
	);

	add_filter( 'map_meta_cap', function ( $caps, $cap, $user_id, $args )
	{
			// Nothing to do
			if( 'delete_post' !== $cap || empty( $args[0] ) )
					return $caps;

			// Target the payment and transaction post types
			if( in_array( get_post_type( $args[0] ), [ 'theme_options'], true ) )
					$caps[] = 'do_not_allow';

			return $caps;
	}, 10, 4 );

	//flush_rewrite_rules( false );

	/*** Fore deleting custom post type
	function delete_post_type(){
	    unregister_post_type( 'blocks' );
	}
	add_action('init','home_sections');
	***/

}
add_action( 'init', 'custom_post_type' );


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'imwp_scripts' );
if ( ! function_exists( 'imwp_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function imwp_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime(get_template_directory() . '/dist/styles/app.bundle.css');
		wp_enqueue_style( 'imwp-styles', get_stylesheet_directory_uri() . '/dist/styles/app.bundle.css', array(), $css_version );


		$js_version = $theme_version . '.' . filemtime(get_template_directory() . '/dist/scripts/app.bundle.js');
		wp_enqueue_script( 'imwp-scripts', get_template_directory_uri() . '/dist/scripts/app.bundle.js', array(), $js_version, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'imwp_scripts' ).


if ( ! function_exists ( 'imwp_setup_theme_default_settings' ) ) {
	function imwp_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$imwp_posts_index_style = get_theme_mod( 'imwp_posts_index_style' );
		if ( '' == $imwp_posts_index_style ) {
			set_theme_mod( 'imwp_posts_index_style', 'default' );
		}

		// Sidebar position.
		$imwp_sidebar_position = get_theme_mod( 'imwp_sidebar_position' );
		if ( '' == $imwp_sidebar_position ) {
			set_theme_mod( 'imwp_sidebar_position', 'right' );
		}

		// Container width.
		$imwp_container_type = get_theme_mod( 'imwp_container_type' );
		if ( '' == $imwp_container_type ) {
			set_theme_mod( 'imwp_container_type', 'container' );
		}
	}
}
