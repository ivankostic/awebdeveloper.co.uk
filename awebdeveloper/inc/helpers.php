<?php
/**
 * Helper functions and definitions
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}


if ( ! function_exists ( 'left_sidebar_check' ) ) {
	function left_sidebar_check() {

    $sidebar_pos = get_theme_mod( 'imwp_sidebar_position' );

    if ( 'left' === $sidebar_pos || 'both' === $sidebar_pos ) {
    	get_template_part( 'layouts/components/sidebar', 'left' );
    }

      // Bootrap & Layout markup after the left sidebar
    	$html = '';
    	if ( 'right' === $sidebar_pos || 'left' === $sidebar_pos ) {
    		$html = '<div class="';
    		if ( ( is_active_sidebar( 'right-sidebar' ) && 'right' === $sidebar_pos ) || ( is_active_sidebar( 'left-sidebar' ) && 'left' === $sidebar_pos ) ) {
    			$html .= 'col-md-8 content-area" id="primary">';
    		} else {
    			$html .= 'col-md-12 content-area" id="primary">';
    		}
    		echo $html; // WPCS: XSS OK.
    	} elseif ( 'both' === $sidebar_pos ) {
    		$html = '<div class="';
    		if ( is_active_sidebar( 'right-sidebar' ) && is_active_sidebar( 'left-sidebar' ) ) {
    			$html .= 'col-md-6 content-area" id="primary">';
    		} elseif ( is_active_sidebar( 'right-sidebar' ) || is_active_sidebar( 'left-sidebar' ) ) {
    			$html .= 'col-md-8 content-area" id="primary">';
    		} else {
    			$html .= 'col-md-12 content-area" id="primary">';
    		}
    		echo $html; // WPCS: XSS OK.
    	} else {
    	    echo '<div class="col-md-12 content-area" id="primary">';
    	}

  }
}


if ( ! function_exists ( 'right_sidebar_check' ) ) {
	function right_sidebar_check() {

    // #closing the primary container
    echo "</div>";

    $sidebar_pos = get_theme_mod( 'imwp_sidebar_position' );

    if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) {
    	get_template_part( 'layouts/components/sidebar', 'right' );
    }

  }
}


/**
 * Count number of widgets in a sidebar
 * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
 */
if ( ! function_exists( 'imwp_slbd_count_widgets' ) ) {
	function imwp_slbd_count_widgets( $sidebar_id ) {
		// If loading from front page, consult $_wp_sidebars_widgets rather than options
		// to see if wp_convert_widget_settings() has made manipulations in memory.
		global $_wp_sidebars_widgets;
		if ( empty( $_wp_sidebars_widgets ) ) :
			$_wp_sidebars_widgets = get_option( 'sidebars_widgets', array() );
		endif;

		$sidebars_widgets_count = $_wp_sidebars_widgets;

		if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) :
			$widget_count = count( $sidebars_widgets_count[ $sidebar_id ] );
			$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );
			if ( $widget_count % 4 == 0 || $widget_count > 6 ) :
				// Four widgets per row if there are exactly four or more than six
				$widget_classes .= ' col-md-6 col-lg-3';
			elseif ( 6 == $widget_count ) :
				// If two widgets are published
				$widget_classes .= ' col-md-2';
			elseif ( $widget_count >= 3 ) :
				// Three widgets per row if there's three or more widgets
				$widget_classes .= ' col-md-4';
			elseif ( 2 == $widget_count ) :
				// If two widgets are published
				$widget_classes .= ' col-md-6';
			elseif ( 1 == $widget_count ) :
				// If just on widget is active
				$widget_classes .= ' col-md-12';
			endif;
			return $widget_classes;
		endif;
	}
}


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
if ( ! function_exists ( 'imwp_categorized_blog' ) ) {
	function imwp_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'imwp_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories( array(
				'fields'     => 'ids',
				'hide_empty' => 1,
				// We only need to know if there is more than one category.
				'number'     => 2,
			) );
			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );
			set_transient( 'imwp_categories', $all_the_cool_cats );
		}
		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so components_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so components_categorized_blog should return false.
			return false;
		}
	}
}


/**
 * Get responsive src and srcset for ACF images
 *
 * @return string
 */
if ( ! function_exists ( 'get_image_src' ) ) {
  function get_image_src($image_id, $image_size) {
    	// check the image ID is not blank
    	if($image_id != '') {
    		// set the default src image size
    		$image_src = wp_get_attachment_image_url( $image_id, $image_size );
    		// set the srcset with various image sizes
    		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
        // get maximum image size
        $image_meta =  wp_get_attachment_metadata( $image_id );
        $max_width = $image_meta[sizes][$image_size][width];

    		return 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
    	}
  }
}

add_filter( 'dynamic_sidebar_params', 'widget_first_last_classes' );
/**
 * Add  numeric index class for each widget (widget-1, widget-2, etc.)
 */
function widget_first_last_classes( $params ) {

	global $my_widget_num; // Global a counter array
	$this_id = $params[0]['id']; // Get the id for the current sidebar we're processing
	$arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets

	if( !$my_widget_num ) {// If the counter array doesn't exist, create it
		$my_widget_num = array();
	}

	// if( !isset( $arr_registered_widgets[$this_id] ) || !is_array( $arr_registered_widgets[$this_id] ) ) { // Check if the current sidebar has no widgets
		// return $params; // No widgets in this sidebar... bail early.
	// }

	if( isset( $my_widget_num[$this_id] ) ) { // See if the counter array has an entry for this sidebar
		$my_widget_num[$this_id] ++;
	} else { // If not, create it starting with 1
		$my_widget_num[$this_id] = 1;
	}

	$class = 'class="widget-' . $my_widget_num[$this_id] . ' '; // Add a widget number class for additional styling options

  if( $my_widget_num[$this_id] == 1 ) {
    // If this is the first widget
    $class .= 'widget_first ';
  } elseif( $my_widget_num[$this_id] == count( $arr_registered_widgets[$this_id] ) ) {
    // If this is the last widget
    $class .= 'widget_last ';
  }

	// $params[0]['before_widget'] = str_replace( 'class="', $class, $params[0]['before_widget'] ); // Insert our new classes into "before widget"
	$params[0]['before_widget'] = preg_replace('/class=\"/', "$class", $params[0]['before_widget'], 1); // Insert our new classes into "before widget"

	return $params;

}


/**
 * Enable shortcodes in text widgets
 *
 * @return string
 */
add_filter('widget_text','do_shortcode');
add_filter( 'widget_text', 'shortcode_unautop');
