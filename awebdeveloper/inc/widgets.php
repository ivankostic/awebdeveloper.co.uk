<?php
/**
 * Declaring widgets
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'widgets_init', 'imwp_widgets_init' );

if ( ! function_exists( 'imwp_widgets_init' ) ) {
	/**
	 * Initializes themes widgets.
	 */
	function imwp_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Right Sidebar', 'imwp' ),
			'id'            => 'right-sidebar',
			'description'   => __( 'Right sidebar widget area', 'imwp' ),
			'before_widget' => '<aside id="%1$s" class="widget right-sidebar-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Left Sidebar', 'imwp' ),
			'id'            => 'left-sidebar',
			'description'   => __( 'Left sidebar widget area', 'imwp' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Top Dynamic', 'imwp' ),
			'id'            => 'top-dynamic',
			'description'   => __( 'Full top widget with dynamic grid', 'imwp' ),
		    'before_widget'  => '<div id="%1$s" class="static-hero-widget %2$s '. imwp_slbd_count_widgets( 'top-dynamic' ) .'">',
		    'after_widget'   => '</div><!-- .static-hero-widget -->',
		    'before_title'   => '<h3 class="widget-title">',
		    'after_title'    => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Dynamic', 'imwp' ),
			'id'            => 'footer-dynamic',
			'description'   => __( 'Full sized footer widget with dynamic grid', 'imwp' ),
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s '. imwp_slbd_count_widgets( 'footer-dynamic' ) .'">',
		    'after_widget'   => '</div><!-- .footer-widget -->',
		    'before_title'   => '<p class="widget-title">',
		    'after_title'    => '</p>',
		) );

	}
} // endif function_exists( 'imwp_widgets_init' ).
