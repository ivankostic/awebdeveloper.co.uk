<?php
/**
 * Custom hooks.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'imwp_site_info' ) ) {
  /**
   * Add site info hook to WP hook library.
   */
  function imwp_site_info() {
    do_action( 'imwp_site_info' );
  }
}

if ( ! function_exists( 'imwp_add_site_info' ) ) {
  add_action( 'imwp_site_info', 'imwp_add_site_info' );

  /**
   * Add site info content.
   */
  function imwp_add_site_info() {
    $year = date("Y");
		$url  = get_bloginfo('url');
		$site = get_bloginfo('name');
		$description = 'All rights reserved.';

    $site_info = sprintf( "&copy; %s <a href='%s'>%s</a>. %s", $year, $url, $site, $description);

    echo apply_filters( 'imwp_site_info_content', $site_info ); // WPCS: XSS ok.
  }
}
