<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div >
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page" itemscope itemtype="http://schema.org/WebSite">
	<?php get_template_part( "layouts/components/header-top-bar" ); ?>
	<header id="masthead" class="site-header section-bg--light">

		<nav class="navbar navbar-expand-md">

		<?php if ( 'container' == $container ) : ?>
			<div class="container" >
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) : ?>

								<h1 class="navbar-brand mb-0">
									<a class="logo logo--header" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"
										 title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
										 itemprop="url"><?php bloginfo( 'url' ); ?></a>
								</h1>
					<?php else:
									the_custom_logo();
								endif;
					?>

				<?php
				$before_menu_html = "<a href='#' class='canvas-close-btn'>&times;</a>" .
														'<a class="logo logo--header" rel="home" href="' . esc_url( home_url( '/' ) ) .'"
																 title="' . esc_attr( get_bloginfo( "name" ) ) . '">' . get_bloginfo( "name" ) . '</a><br>' .
														'<p class="site-description">' . esc_attr( get_bloginfo( "name", "display" ) ) . '</p>';

				$after_menu_html = '<a class="btn btn-success" href="' . do_shortcode( '[options field=get_in_touch_link]' ) . '
													" title="Get in touch">Hire me!</a>';

				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'canvas-nav',
						'menu_class'      => 'navbar-nav',
						'menu_id'         => 'main-menu',
						'depth'           => 4,
						'walker'          => new imwp_WP_Bootstrap_Navwalker($before_menu_html, $after_menu_html),
					)
				); ?>

				<div class="header-info">
					<a class="btn btn-success" href="<?= do_shortcode( '[options field=get_in_touch_link]' ); ?>" title="Get in touch">Get in touch</a>
				</div>

				<button class="navbar-toggler"
								type="button"
								aria-label="<?php esc_attr_e( 'Toggle navigation', 'imwp' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<button class="fixed-toggler" type="button"
					aria-label="<?php esc_attr_e( 'Toggle navigation', 'imwp' ); ?>">
					<span class="fixed-toggler__icon"></span>
				</button>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->
	</header>

  <div class="nav-overlay"></div>
	<main id="main" class="site-main">
