<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="col-md-4 widget-area" role="complementary" id="secondary">
	blabla
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
