<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'imwp_container_type' );
?>

</main><!-- #main -->

<?php get_template_part( 'layouts/components/sidebar', 'footer-dynamic' ); ?>

<footer class="site-footer" id="colophon">
	<div class="<?php echo esc_attr( $container ); ?>">
		<div class="row">
			<div class="col-md-12">

					<div class="site-info">

						<?php imwp_site_info(); ?>

					</div><!-- .site-info -->
			</div><!--col end -->
		</div><!-- row end -->
	</div><!-- .container -->
</footer><!-- #colophon -->

</div><!-- #page we need this extra closing tag here -->
<?php
$ga_code = do_shortcode( '[options field=ga_code]' );
echo $ga_code ? $ga_code : "";
?>
<?php wp_footer(); ?>
<?php?>
</body>
</html>
