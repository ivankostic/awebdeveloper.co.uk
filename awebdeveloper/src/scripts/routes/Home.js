// JS components
import CanvasStars from '../components/CanvasStars'

export default {
  init() {
    // scripts here run on the DOM load event
    CanvasStars.init();
    // console.log('Home JS');
  },
  finalize() {
    // scripts here fire after init() runs
  },
};
