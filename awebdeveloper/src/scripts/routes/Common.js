// JS components
import Navigation from '../components/Navigation'
import SmoothScroll from '../components/SmoothScroll'

export default {
  init() {
    // scripts here run on the DOM load event
    Navigation.init();
    SmoothScroll.init();
    // console.log('This is Common JS.');
    console.log('Hooray, no JavaSrcipt errors in production! Did I get the job? :-D');
  },
  finalize() {
    // scripts here fire after init() runs
  },
};
