export default {
  init() {
    let el = $('.right-sidebar-widget.widget_last');
    let sticky_navigation_offset_top = el.offset().top;

    let sticky_navigation = function(){

      let scroll_top = $(window).scrollTop(); // our current vertical position from the top

      // if we've scrolled more than the navigation, change its position to fixed to stick to top,
      // otherwise change it back to relative
      if (scroll_top > sticky_navigation_offset_top && $('body').width() > "768") {
          el.addClass('sticky');
      } else {
          el.removeClass('sticky');
      }

    };

    // run our function on load
    sticky_navigation();

    // and run it again every time you scroll
   $(window).scroll(() => {
       sticky_navigation();
    });

    $(window).resize(() => {
        $('.right-sidebar-widget.widget_last').removeClass('sticky');
     });

  },
}
