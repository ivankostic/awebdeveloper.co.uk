export default {
  init() {

    // Navbar canvas toggle
    $('.navbar-toggler, .canvas-close-btn, .nav-overlay, .fixed-toggler').click((e) => {
      e.preventDefault();
      toggleNav();
    });

    // Enable clicks on dropdown items
    $('.dropdown-toggle').click((e) => {
        location.href = $(this).attr('href');
    });

    // Things to do & check on browser resize
    $( window ).resize((e) => {
      if ($('body').hasClass('show-nav')) {
        toggleNav();
      }
    });

    let shouldMenuBeFixed = () => {
      if ( isVisible( $(".navbar-brand") ) || isVisible( $(".site-footer") )  ) {
        $('.fixed-toggler').removeClass('fixed-toggler--active');
      } else {
        $('.fixed-toggler').addClass('fixed-toggler--active');
      }
    }

    $(window).scroll(function() {
      shouldMenuBeFixed();
    });
    shouldMenuBeFixed();

  },
}

let toggleNav = () => {
  $('body').toggleClass('show-nav');
  window.getSelection().removeAllRanges();
}

let isVisible = (el) => {
  var top_of_element = el.offset().top;
  var bottom_of_element = el.offset().top + el.outerHeight();
  var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
  var top_of_screen = $(window).scrollTop();

  if( (bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element) ){
    return true;
  }
  else {
    return false;
  }
}
