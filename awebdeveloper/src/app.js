import './scripts/BootstrapLoader';
import './styles/app.scss'

// Set Routes to load JS on specific body classes
import Router from './scripts/util/Router';
import common from './scripts/routes/Common';
import home from './scripts/routes/Home';
import singlePost from './scripts/routes/Single';
import archive from './scripts/routes/Archive';
import pageTemplateDefault from './scripts/routes/Page';

// Populate Router instance with DOM routes
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // Other pages
  pageTemplateDefault,
  singlePost,
  archive,
});

// Load Events
$(document).ready(() => routes.loadEvents());
