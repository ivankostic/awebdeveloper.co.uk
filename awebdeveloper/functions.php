<?php
/**
 * imwp functions and definitions
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Helper functions.
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Custom shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';


/**
 * Register widget area.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/inc/vendors/class-wp-bootstrap-navwalker.php';

/**
 * Import ACF fields from earlier exported PHP.
 */
require get_template_directory() . '/inc/acf-sync.php';


/**
 * Load WooCommerce functions.
 */
// require get_template_directory() . '/inc/woocommerce.php';
