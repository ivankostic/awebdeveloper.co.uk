<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'imwp_container_type' );
$sidebar_pos = get_theme_mod( 'imwp_sidebar_position' );
?>
<div class="<?php echo esc_attr( $container ); ?>"  tabindex="-1">
	<div class="row">
		<div class="col-md-12 content-area" id="primary">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.',
					'imwp' ); ?></h1>
				</header><!-- .page-header -->
				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?',
					'imwp' ); ?></p>

					<?php get_search_form(); ?>

					<br>

					<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

					<?php if ( imwp_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
						<div class="widget widget_categories">

							<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'imwp' ); ?></h2>

							<ul>
								<?php
								wp_list_categories( array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								) );
								?>
							</ul>

						</div><!-- .widget -->
					<?php endif; ?>

					<?php
					/* translators: %1$s: smiley */
					$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'imwp' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</div><!-- #primary -->
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
